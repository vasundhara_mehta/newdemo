
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"

#import "WebManager.h"
#import "DataHolder.h"
@implementation WebManager

@synthesize receivedData;
@synthesize delegate;
@synthesize callback;
@synthesize errorCallback,accessToken;

static WebManager *WebManagerSharedInstance;

- (id) init
{
	if (self = [super init])
	{
		receivedData	= nil;
		delegate		= nil;
		callback		= nil;
		errorCallback	= nil;
        //UID= [[UIDevice currentDevice] uniqueIdentifier];
	}
	return self;
}
+ (WebManager *) WebManagerSharedInstance
{
	@synchronized ([WebManager class])
	{
		if (!WebManagerSharedInstance)
		{
			WebManagerSharedInstance = [[WebManager alloc] init];
            
		}
		return WebManagerSharedInstance;
	}
	return nil;
}

-(void)FetchUserObjectsWithUserID:(NSString*)ID Delegate:(id)requestDelegate
                   WithSelector:(SEL)requestSelector
              WithErrorSelector:(SEL)requestErrorSelector{
    delegate    = requestDelegate;
	callback    = requestSelector;
	errorCallback=requestErrorSelector;
    // requestBody=[NSMutableString stringWithFormat:@""];
    
    NSString *urlString=[NSString stringWithFormat:@"https://api.instagram.com/v1/users/%@?access_token=%@",ID, [DataHolder DataHolderSharedInstance].AccessToken];
    NSLog(@"Request: %@", urlString);
    NSURL* url = [NSURL URLWithString:urlString];
   	theRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1000.0];
    [self Check_Network_Status_And_Move_On];
    
}

-(void)FetchMediaObjectsWithTag:(NSString*)tag Delegate:(id)requestDelegate
                     WithSelector:(SEL)requestSelector
                WithErrorSelector:(SEL)requestErrorSelector{
    delegate    = requestDelegate;
	callback    = requestSelector;
	errorCallback=requestErrorSelector;
   // requestBody=[NSMutableString stringWithFormat:@""];
    
    NSString *urlString=[NSString stringWithFormat:@"https://api.instagram.com/v1/tags/%@/media/recent?access_token=%@",tag, [DataHolder DataHolderSharedInstance].AccessToken];
    NSLog(@"Request: %@", urlString);
    NSURL* url = [NSURL URLWithString:urlString];
   	theRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1000.0];
    [self Check_Network_Status_And_Move_On];
    
}

-(void)FetchMediaObjectsWithUSerId:(NSString*)USerId Delegate:(id)requestDelegate
                   WithSelector:(SEL)requestSelector
              WithErrorSelector:(SEL)requestErrorSelector{
    delegate    = requestDelegate;
	callback    = requestSelector;
	errorCallback=requestErrorSelector;
    // requestBody=[NSMutableString stringWithFormat:@""];
    
    NSString *urlString=[NSString stringWithFormat:@"https://api.instagram.com/v1/users/%@/media/recent?max_id=%@&access_token=%@",USerId,[DataHolder DataHolderSharedInstance].NextMaxId, [DataHolder DataHolderSharedInstance].AccessToken];
    NSLog(@"Request: %@", urlString);
    NSURL* url = [NSURL URLWithString:urlString];
   	theRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1000.0];
    [self Check_Network_Status_And_Move_On];
    
}
-(void)FetchMediaObjectsWithMediaId:(NSString*)mediaId Delegate:(id)requestDelegate
WithSelector:(SEL)requestSelector
WithErrorSelector:(SEL)requestErrorSelector{
    delegate    = requestDelegate;
	callback    = requestSelector;
	errorCallback=requestErrorSelector;
    // requestBody=[NSMutableString stringWithFormat:@""];
    
    NSString *urlString=[NSString stringWithFormat:@"https://api.instagram.com/v1/media/%@?access_token=%@",mediaId, [DataHolder DataHolderSharedInstance].AccessToken];
    NSLog(@"Request: %@", urlString);
    NSURL* url = [NSURL URLWithString:urlString];
   	theRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1000.0];
    [self Check_Network_Status_And_Move_On];
    
}

-(void)SignUpUserWithDelegate:(id)requestDelegate
                WithSelector:(SEL)requestSelector
           WithErrorSelector:(SEL)requestErrorSelector{
    delegate    = requestDelegate;
	callback    = requestSelector;
	errorCallback=requestErrorSelector;
    
    NSString *urlString=[NSString stringWithFormat:@"http://162.242.152.155/Didle_Doo/webservices/app_registerd_user.php"];
    UserProfile *userprofile=[DataHolder DataHolderSharedInstance].UserProfile;
    requestBody=[[NSMutableString alloc]initWithFormat:@"action=user_profile"];
    [requestBody appendFormat:@"&instagram_id=%@",[DataHolder DataHolderSharedInstance].UserID];
    [requestBody appendFormat:@"&full_name=%@",userprofile.full_name];
    [requestBody appendFormat:@"&bio=%@",userprofile.bio];
    [requestBody appendFormat:@"&website=%@",userprofile.website];
    [requestBody appendFormat:@"&profile_picture=%@",userprofile.profile_picture];
    [requestBody appendFormat:@"&gender=%@",[DataHolder DataHolderSharedInstance].UserProfile.gender];
    [requestBody appendFormat:@"&category=1"];
    [requestBody appendFormat:@"&media=%@",userprofile.media];
    [requestBody appendFormat:@"&followed_by=%@",userprofile.followed_by];
    [requestBody appendFormat:@"&follows=%@",userprofile.follows];
   NSData *requestData = [NSData dataWithBytes:[requestBody UTF8String] length:[requestBody length]];
	NSString *msgLength = [NSString stringWithFormat:@"%d", [requestBody length]];
	NSURL* url = [NSURL URLWithString:urlString];
   	theRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1000.0];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
	[theRequest setHTTPMethod:@"POST"];
	[theRequest setHTTPBody: requestData];
    [self Check_Network_Status_And_Move_On];
    
}

-(void)LogInUserWithDelegate:(id)requestDelegate
                 WithSelector:(SEL)requestSelector
            WithErrorSelector:(SEL)requestErrorSelector{
    delegate    = requestDelegate;
	callback    = requestSelector;
	errorCallback=requestErrorSelector;
    
    NSString *urlString=[NSString stringWithFormat:@"http://162.242.152.155/Didle_Doo/webservices/app_login_user.php"];
    requestBody=[[NSMutableString alloc]initWithFormat:@"action=user_login"];
    [requestBody appendFormat:@"&instagram_id=%@",[DataHolder DataHolderSharedInstance].UserID];
    [requestBody appendFormat:@"&media=%@",[DataHolder DataHolderSharedInstance].UserProfile.media];
    [requestBody appendFormat:@"&followed_by=%@",[DataHolder DataHolderSharedInstance].UserProfile.followed_by];
    [requestBody appendFormat:@"&follows=%@",[DataHolder DataHolderSharedInstance].UserProfile.follows];
    
    
    NSData *requestData = [NSData dataWithBytes:[requestBody UTF8String] length:[requestBody length]];
	NSString *msgLength = [NSString stringWithFormat:@"%d", [requestBody length]];
	NSURL* url = [NSURL URLWithString:urlString];
   	theRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1000.0];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
	[theRequest setHTTPMethod:@"POST"];
	[theRequest setHTTPBody: requestData];
    [self Check_Network_Status_And_Move_On];
    
}

-(void)UserTimeStampWithDelegate:(id)requestDelegate
                WithSelector:(SEL)requestSelector
           WithErrorSelector:(SEL)requestErrorSelector{
    delegate    = requestDelegate;
	callback    = requestSelector;
	errorCallback=requestErrorSelector;
    
    NSString *urlString=[NSString stringWithFormat:@"http://162.242.152.155/Didle_Doo/webservices/app_user_session_stop.php"];
    requestBody=[[NSMutableString alloc]initWithFormat:@"action=user_session_stop"];
    [requestBody appendFormat:@"&instagram_id=%@",[DataHolder DataHolderSharedInstance].UserID];
    [requestBody appendFormat:@"&start_time=%@",[DataHolder DataHolderSharedInstance].StartTime];
    [requestBody appendFormat:@"&end_time=%@",[DataHolder DataHolderSharedInstance].EndTime];
    
    [requestBody appendFormat:@"&app_likes=%d",[DataHolder DataHolderSharedInstance].SessionLikes];
    [requestBody appendFormat:@"&app_comments=%d",[DataHolder DataHolderSharedInstance].SessionComments];
    [requestBody appendFormat:@"&app_followers=%d",[DataHolder DataHolderSharedInstance].SessionFollows];

    
    [requestBody appendFormat:@"&user_media=%@",[DataHolder DataHolderSharedInstance].UserProfile.media];
    [requestBody appendFormat:@"&user_followed_by=%@",[DataHolder DataHolderSharedInstance].UserProfile.followed_by];
    [requestBody appendFormat:@"&user_follows=%@",[DataHolder DataHolderSharedInstance].UserProfile.follows];

    
    
    NSData *requestData = [NSData dataWithBytes:[requestBody UTF8String] length:[requestBody length]];
	NSString *msgLength = [NSString stringWithFormat:@"%d", [requestBody length]];
	NSURL* url = [NSURL URLWithString:urlString];
   	theRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1000.0];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
	[theRequest setHTTPMethod:@"POST"];
	[theRequest setHTTPBody: requestData];
    [self Check_Network_Status_And_Move_On];
    
}

-(void)GetCategoriesWithDelegate:(id)requestDelegate
                WithSelector:(SEL)requestSelector
           WithErrorSelector:(SEL)requestErrorSelector{
    delegate    = requestDelegate;
	callback    = requestSelector;
	errorCallback=requestErrorSelector;
    
    NSString *urlString=[NSString stringWithFormat:@"http://162.242.152.155/Didle_Doo/webservices/app_login_user.php"];
    requestBody=[[NSMutableString alloc]initWithFormat:@"action=get_tags"];
    NSData *requestData = [NSData dataWithBytes:[requestBody UTF8String] length:[requestBody length]];
	NSString *msgLength = [NSString stringWithFormat:@"%d", [requestBody length]];
	NSURL* url = [NSURL URLWithString:urlString];
   	theRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1000.0];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
	[theRequest setHTTPMethod:@"POST"];
	[theRequest setHTTPBody: requestData];
    [self Check_Network_Status_And_Move_On];
    
}

-(void)UpdateTagsWithDelegate:(id)requestDelegate
                    WithSelector:(SEL)requestSelector
               WithErrorSelector:(SEL)requestErrorSelector{
    delegate    = requestDelegate;
	callback    = requestSelector;
	errorCallback=requestErrorSelector;
    
    NSMutableString *Tags=[[NSMutableString alloc] init];
    for (int i=0;i<[DataHolder DataHolderSharedInstance].userSelectedTags.count;i++) {
        NSString *str=[[DataHolder DataHolderSharedInstance].userSelectedTags objectAtIndex:i];
        if (i<[DataHolder DataHolderSharedInstance].userSelectedTags.count-1) {
            [Tags appendFormat:@"%@;",str];
        }
        else
            [Tags appendFormat:@"%@",str];
    
    }
    NSString *urlString=[NSString stringWithFormat:@"http://162.242.152.155/Didle_Doo/webservices/app_update_user_tags.php"];
    requestBody=[[NSMutableString alloc]initWithFormat:@"action=user_tags_update"];
    [requestBody appendFormat:@"&instagram_id=%@",[DataHolder DataHolderSharedInstance].UserID];
    [requestBody appendFormat:@"&user_tags=%@",Tags];
    
    NSData *requestData = [NSData dataWithBytes:[requestBody UTF8String] length:[requestBody length]];
	NSString *msgLength = [NSString stringWithFormat:@"%d", [requestBody length]];
	NSURL* url = [NSURL URLWithString:urlString];
   	theRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1000.0];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
	[theRequest setHTTPMethod:@"POST"];
	[theRequest setHTTPBody: requestData];
    [self Check_Network_Status_And_Move_On];
    
}

-(void)LogoutUserWithDelegate:(id)requestDelegate
                 WithSelector:(SEL)requestSelector
            WithErrorSelector:(SEL)requestErrorSelector{
    delegate    = requestDelegate;
	callback    = requestSelector;
	errorCallback=requestErrorSelector;
    
    NSString *urlString=[NSString stringWithFormat:@"http://162.242.152.155/Didle_Doo/webservices/app_logout_user.php"];
    requestBody=[[NSMutableString alloc]initWithFormat:@"action=user_logout"];
    [requestBody appendFormat:@"&instagram_id=%@",[DataHolder DataHolderSharedInstance].UserID];
    
    
    NSData *requestData = [NSData dataWithBytes:[requestBody UTF8String] length:[requestBody length]];
	NSString *msgLength = [NSString stringWithFormat:@"%d", [requestBody length]];
	NSURL* url = [NSURL URLWithString:urlString];
   	theRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1000.0];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
	[theRequest setHTTPMethod:@"POST"];
	[theRequest setHTTPBody: requestData];
    [self Check_Network_Status_And_Move_On];
    
}


-(void)LikeMediaObjectWithID:(NSString*)ID Delegate:(id)requestDelegate
                   WithSelector:(SEL)requestSelector
              WithErrorSelector:(SEL)requestErrorSelector{
    delegate    = requestDelegate;
	callback    = requestSelector;
	errorCallback=requestErrorSelector;
    
    NSString *urlString=[NSString stringWithFormat:@"https://api.instagram.com/v1/media/%@/likes?access_token=%@",ID, [DataHolder DataHolderSharedInstance].AccessToken];
    NSLog(@"Request: %@", urlString);
    NSURL* url = [NSURL URLWithString:urlString];
   	theRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1000.0];
    [theRequest setHTTPMethod:@"POST"];
    [self Check_Network_Status_And_Move_On];
    
}
-(void)FollowUserWithID:(NSString*)ID Delegate:(id)requestDelegate
                WithSelector:(SEL)requestSelector
           WithErrorSelector:(SEL)requestErrorSelector{
    delegate    = requestDelegate;
	callback    = requestSelector;
	errorCallback=requestErrorSelector;
    
    NSString *urlString=[NSString stringWithFormat:@"https://api.instagram.com/v1/users/%@/relationship?access_token=%@",ID, [DataHolder DataHolderSharedInstance].AccessToken];
    NSLog(@"Request: %@", urlString);
    NSURL* url = [NSURL URLWithString:urlString];
   	theRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1000.0];
    NSString *Param=@"action=follow";
    [theRequest setHTTPBody:[Param dataUsingEncoding:NSUTF8StringEncoding]];
    [theRequest setHTTPMethod:@"POST"];
    [self Check_Network_Status_And_Move_On];
    
}
-(void)ValidateTagWithTagName:(NSString*)name Delegate:(id)requestDelegate
           WithSelector:(SEL)requestSelector
      WithErrorSelector:(SEL)requestErrorSelector{
    delegate    = requestDelegate;
	callback    = requestSelector;
	errorCallback=requestErrorSelector;
    
    NSString *urlString=[NSString stringWithFormat:@"https://api.instagram.com/v1/tags/%@?access_token=%@",name, [DataHolder DataHolderSharedInstance].AccessToken];
    NSLog(@"Request: %@", urlString);
    NSURL* url = [NSURL URLWithString:urlString];
   	theRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:1000.0];
    [theRequest setHTTPMethod:@"GET"];
    [self Check_Network_Status_And_Move_On];
    
}
- (void) connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]
             forAuthenticationChallenge:challenge];
    }
    
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

- (void) connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
    if([[protectionSpace authenticationMethod] isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        return;
    }
}

-(void)Check_Network_Status_And_Move_On
{
    if ([self Is_Network_Reachable] == YES)
    {
        [self CreateConnectionAndStartDownloading];
    }
    else
    {
        [[AppManager AppManagerSharedInstance] Hide_Network_Indicator];
        [[AppManager AppManagerSharedInstance] Hide_Waiting_Alert];
        
        UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:@"No Connection" message:@"The internet connection appears to be offline.\n or slow connectivity signals." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        Alert.tag = 0;
        [Alert show];
        if(errorCallback)
            [delegate performSelector:errorCallback withObject:nil];
    }
}
-(void)CreateConnectionAndStartDownloading;
{
    // NSLog(@"%@",requestBody);
   
	theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    if (requestBody) {
        requestBody=nil;
    }
    if (theConnection)
		receivedData = [NSMutableData data];
	else
		NSLog(@"Connection Failed");
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[receivedData appendData:data];
   // NSString *a = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	[receivedData setLength:0];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	if(delegate && callback){
		if([delegate respondsToSelector:callback])
        {
			[delegate performSelector:callback withObject:receivedData];
		}
        else
        {
			NSLog(@"No response from delegate");
            if(errorCallback)
                [delegate performSelector:errorCallback withObject:nil];
        }
    }
}
- (void)connection:(NSURLConnection *)connection  didFailWithError:(NSError *)error
{
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
	
	receivedData=nil;
	theConnection=nil;
	if(errorCallback)
		[delegate performSelector:errorCallback withObject:error];
}
-(BOOL)Is_Network_Reachable
{
	Reachability * reach1 = [Reachability reachabilityForInternetConnection];
	Reachability * reach2 = [Reachability reachabilityForLocalWiFi];
	NetworkStatus netstatus1 = [reach1 currentReachabilityStatus];
	NetworkStatus netstatus2 = [reach2 currentReachabilityStatus];
	if((netstatus1 == NotReachable) && (netstatus2 == NotReachable))
		return NO;
	return YES;
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1111) // NETWORK SLOW -> PENDING SOAP REQUEST : LOGIN
    {
        if (buttonIndex == 0)
        {
            [[AppManager AppManagerSharedInstance] Show_Network_Indicator];
            [[AppManager AppManagerSharedInstance] Show_Waiting_Alert];
            [self Check_Network_Status_And_Move_On];
        }
    }
}
-(void)Reset
{
	delegate    = nil;
	callback    = nil;
	errorCallback = nil;
}
-(void) dealloc
{
	
	receivedData=nil;
	theConnection=nil;
}







@end