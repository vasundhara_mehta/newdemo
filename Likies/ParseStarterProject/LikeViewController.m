//
//  LikeViewController.m
//  ParseStarterProject
//
//  Created by Kanwal on 4/21/14.
//
//

#import "LikeViewController.h"
#import "DataHolder.h"
#import "MediaObject.h"
#import "WebManager.h"
#import "JSONParser.h"
#import "CustomCell.h"
#import "UIImageView+WebCache.h"
#import "GetLikesViewController.h"
#import "InAppPurchaseViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ParseStarterProjectAppDelegate.h"
#import "PHPublisherContentRequest.h"
#define PLAYHAVAN_TOKEN @"74b642ad17ef4cce8043147a46f9708b"
#define PLAYHAVAN_SECRET @"789dc84d06e6484c8147f067953f8fc6"

@interface LikeViewController ()

@end

@implementation LikeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([UIScreen mainScreen].bounds.size.height==568) {
        getcoinsBtn.frame=CGRectMake(0, 390, 160, 49);
        getlikesBtn.frame=CGRectMake(160, 390, 160, 49);
        //getlikesView.frame=CGRectMake(0, 50, 320, 400);
    }
    act.hidden=YES;
       getcoinsBtn.enabled=NO;
    [self GetImageFromArray];
    //coinsLabel.layer.borderColor = [UIColor blueColor].CGColor;
    //coinsLabel.layer.borderWidth = 1.0;
//    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
//        coinsLabel.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"coinback_ipad.png"]];
//
//    }else{
    coinsLabel.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"coinback.png"]];
  //  }
    coinsLabel.layer.cornerRadius = 10;
    likeBtn.titleLabel.font = [UIFont fontWithName:@"Dosis-Light" size:18];
    skipBtn.titleLabel.font  = [UIFont fontWithName:@"Dosis-Light" size:18];
    // Do any additional setup after loading the view from its nib.
    
    CGPoint origin ;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        origin= CGPointMake(215.0,
                            self.view.frame.size.height -
                            CGSizeFromGADAdSize(kGADAdSizeBanner).height);

    }else{
    origin= CGPointMake(0.0,
                                 self.view.frame.size.height -
                                 CGSizeFromGADAdSize(kGADAdSizeBanner).height);
    }
    self.bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner origin:origin];
    self.bannerView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    
    // Note: Edit SampleConstants.h to provide a definition for kSampleAdUnitID before compiling.
    self.bannerView.adUnitID = Admob_APPID;
    self.bannerView.delegate = self;
    self.bannerView.rootViewController = self;
    [self.view addSubview:self.bannerView];
    [self.bannerView loadRequest:[self request]];
}



#pragma mark GADRequest generation

- (GADRequest *)request {
    GADRequest *request = [GADRequest request];
    
    // Make the request for a test ad. Put in an identifier for the simulator as well as any devices
    // you want to receive test ads.
//    request.testDevices = @[
//                            // TODO: Add your device/simulator test identifiers here. Your device identifier is printed to
//                            // the console when the app is launched.
//                            GAD_SIMULATOR_ID
//                            ];
    return request;
}



- (void)adViewDidReceiveAd:(GADBannerView *)view {
    NSLog(@"Ads----------received");
    
}





// Sent when an ad request failed.  Normally this is because no network
// connection was available or no ads were available (i.e. no fill).  If the
// error was received as a part of the server-side auto refreshing, you can
// examine the hasAutoRefreshed property of the view.
- (void)adView:(GADBannerView *)view
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"error %@",error.localizedDescription);
}
-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    [self UpdateCoinsLabel];
    [self HideGetLikes];
}
-(IBAction)GetImagesToLikedFromParse{

    act.hidden=NO;
    reloadBtn.hidden=YES;
    likeBtn.enabled=NO;
    skipBtn.enabled=NO;
    self.view.userInteractionEnabled=NO;
    PFQuery *query = [PFQuery queryWithClassName:@"MediaLikes"];
    [query whereKey:@"liked" notEqualTo:[DataHolder DataHolderSharedInstance].UserID];
    [query whereKey:@"skip" notEqualTo:[DataHolder DataHolderSharedInstance].UserID];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully retrieved %d scores.", objects.count);
            [[DataHolder DataHolderSharedInstance].OBjectsTobeLiked removeAllObjects];
            [[DataHolder DataHolderSharedInstance].OBjectsTobeLiked addObjectsFromArray:objects];
            [self GetImageFromArray];
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
            //[self RegisterUser];
        }
    }];

}
-(void)GetImageFromArray{

    //act.hidden=NO;
    self.view.userInteractionEnabled=NO;
    likeBtn.enabled=NO;
    skipBtn.enabled=NO;
    if ([DataHolder DataHolderSharedInstance].OBjectsTobeLiked.count>0) {
        PFObject *Object=[[DataHolder DataHolderSharedInstance].OBjectsTobeLiked objectAtIndex:0];
        [Object refresh];
        NSArray *arr=[Object objectForKey:@"liked"];
        if ([[Object objectForKeyedSubscript:@"likesDue"] integerValue]==arr.count) {
        
            [[DataHolder DataHolderSharedInstance].OBjectsTobeLiked removeObjectAtIndex:0];
            [self GetImageFromArray];
        }
        NSString *imageID=[Object objectForKey:@"mediaId"];
        [[WebManager WebManagerSharedInstance] FetchMediaObjectsWithMediaId:imageID Delegate:self WithSelector:@selector(MediaFetched:) WithErrorSelector:@selector(Error:)];
        
        
    }
    else{
    
        act.hidden=YES;
        reloadBtn.hidden=NO;
        self.view.userInteractionEnabled=YES;
        
    }
}
-(void)MediaFetched:(NSData*)data{

    NSString *a = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
    NSLog(@" Media Data: %@", a);
    MediaObject *obj=[[JSONParser JSONParserSharedInstance] ParseMediaObject:data];
    NSLog(@" link: %@", obj.low_resolution);
    [self loadFromURL:[NSURL URLWithString:obj.low_resolution] callback:^(UIImage *image) {
        ImageView.image=image;
        likeBtn.enabled=YES;
        skipBtn.enabled=YES;
        act.hidden=YES;
    }];
    self.view.userInteractionEnabled=YES;

}
-(IBAction)LikeCurrentMedia{

    act.hidden=NO;
    likeBtn.enabled=NO;
    skipBtn.enabled=NO;
    if ([DataHolder DataHolderSharedInstance].OBjectsTobeLiked.count>0) {
    PFObject *Object=[[DataHolder DataHolderSharedInstance].OBjectsTobeLiked objectAtIndex:0];
    [[WebManager WebManagerSharedInstance] LikeMediaObjectWithID:[Object objectForKey:@"mediaId"] Delegate:self WithSelector:@selector(Liked:) WithErrorSelector:@selector(Error:)];
    }
}
-(IBAction)SkipCurrentMedia{
    act.hidden=NO;
    likeBtn.enabled=NO;
    skipBtn.enabled=NO;
    PFObject *Object=[[DataHolder DataHolderSharedInstance].OBjectsTobeLiked objectAtIndex:0];
    [Object refresh];
    NSMutableArray *arr=[[NSMutableArray alloc] initWithArray:[Object objectForKey:@"skip"]];
    [arr addObject:[DataHolder DataHolderSharedInstance].UserID];
    Object[@"skip"]=arr;
    [Object saveInBackground];
    [[DataHolder DataHolderSharedInstance].OBjectsTobeLiked removeObjectAtIndex:0];
    if ([DataHolder DataHolderSharedInstance].OBjectsTobeLiked.count==0) {
        [self GetImagesToLikedFromParse];
    }
    [self GetImageFromArray];
}
-(void)Liked:(NSData*)data{

    NSString *a = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
    NSLog(@" liked Data: %@", a);
    
    PFObject *Object=[[DataHolder DataHolderSharedInstance].OBjectsTobeLiked objectAtIndex:0];
    [Object refresh];
    NSMutableArray *arr=[[NSMutableArray alloc] initWithArray:[Object objectForKey:@"liked"]];
    [arr addObject:[DataHolder DataHolderSharedInstance].UserID];
    Object[@"liked"]=arr;
    [Object saveInBackground];
    PFQuery *query = [PFQuery queryWithClassName:@"user"];
    [query getObjectInBackgroundWithId:[DataHolder DataHolderSharedInstance].UserObjectID block:^(PFObject *user, NSError *error) {
        // Do something with the returned PFObject in the gameScore variable.
        int coins=[[user objectForKey:@"coins"] integerValue]+1;
        user[@"coins"]=[NSNumber numberWithInt:coins];
        [user save];
    }];
    
         [self UpdateCoinsLabel];
    [[DataHolder DataHolderSharedInstance].OBjectsTobeLiked removeObjectAtIndex:0];
    if ([DataHolder DataHolderSharedInstance].OBjectsTobeLiked.count==0) {
        [self GetImagesToLikedFromParse];
    }
    [self GetImageFromArray];

    
}
-(void)UpdateCoinsLabel{

    PFQuery *query = [PFQuery queryWithClassName:@"user"];
    [query getObjectInBackgroundWithId:[DataHolder DataHolderSharedInstance].UserObjectID block:^(PFObject *user, NSError *error) {
        // Do something with the returned PFObject in the gameScore variable.
        int coins=[[user objectForKey:@"coins"] integerValue];
        coinsLabel.text=[NSString stringWithFormat:@" %d",coins];
    }];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) loadFromURL: (NSURL*) url callback:(void (^)(UIImage *image))callback {
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        NSData * imageData = [NSData dataWithContentsOfURL:url];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *image = [UIImage imageWithData:imageData];
            callback(image);
        });
    });
}
-(void)Error:(NSError*)error{
    self.view.userInteractionEnabled=YES;
    act2.hidden=YES;
    reloadUserMedia.hidden=NO;
    act.hidden=YES;
}
//playhaven delegete

- (void)requestDidGetContent:(PHPublisherContentRequest *)request{
    
}
- (void)request:(PHPublisherContentRequest *)request contentDidDisplay:(PHContent *)content{
    
}
- (void)request:(PHPublisherContentRequest *)request contentDidDismissWithType:(PHPublisherContentDismissType *)type{
    
}
- (void)request:(PHPublisherContentRequest *)request didFailWithError:(NSError *)error{
    NSLog(@"errrroooorrr %@",error.localizedDescription);
}
//playhaven delegate end

-(IBAction)ShowGetLikes{
    if(![[NSUserDefaults standardUserDefaults]boolForKey:@"removeads"]){
    //playhaven
    PHPublisherContentRequest *request = [PHPublisherContentRequest requestForApp:PLAYHAVAN_TOKEN secret:PLAYHAVAN_SECRET placement:@"main_menu" delegate:self];
    //    PHPublisherContentRequest *request=[PHPublisherContentRequest requestForApp:PLAYHAVAN_TOKEN secret:PLAYHAVAN_SECRET];
    //    request.delegate=self;
    [request send];
    }
    [UIView beginAnimations:@"share_out" context:nil];
	[UIView setAnimationBeginsFromCurrentState:YES];
  	[UIView setAnimationRepeatCount:0];
	[UIView setAnimationDuration:0.5];
	getlikesView.frame=CGRectMake(0, getlikesView.frame.origin.y, getlikesView.frame.size.width, getlikesView.frame.size.height);
	[UIView commitAnimations];
    getlikesBtn.enabled=NO;
    getcoinsBtn.enabled=YES;
    [self GetUserMedia];
    

}
-(IBAction)HideGetLikes{

    [UIView beginAnimations:@"share_out" context:nil];
	[UIView setAnimationBeginsFromCurrentState:YES];
  	[UIView setAnimationRepeatCount:0];
	[UIView setAnimationDuration:0.5];
	getlikesView.frame=CGRectMake(getlikesView.frame.size.width, getlikesView.frame.origin.y, getlikesView.frame.size.width, getlikesView.frame.size.height);
	[UIView commitAnimations];
    getlikesBtn.enabled=YES;
    getcoinsBtn.enabled=NO;
    
}
-(IBAction)GetUserMedia{

    self.view.userInteractionEnabled=NO;
    act2.hidden=NO;
    reloadUserMedia.hidden=YES;
    [[DataHolder DataHolderSharedInstance].MediaObjectsArray removeAllObjects];
    [self GetMedia];
    
}
-(void)GetMedia{

    [[WebManager WebManagerSharedInstance] FetchMediaObjectsWithUSerId:[DataHolder DataHolderSharedInstance].UserID Delegate:self WithSelector:@selector(MediaFectched:) WithErrorSelector:@selector(Error:)];
}
-(void)MediaFectched:(NSData*)data{

    [[JSONParser JSONParserSharedInstance] ParseMediaObjects:data];
    if (![[DataHolder DataHolderSharedInstance].NextMaxId isEqualToString:@""]) {
        [self GetMedia];
    }
    else{
    
        
        [self GetAccomlishedArray];
    }
    
}
-(void)GetAccomlishedArray{

    PFQuery *query = [PFQuery queryWithClassName:@"MediaLikes"];
    [query whereKey:@"userId" equalTo:[DataHolder DataHolderSharedInstance].UserID];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully Accomplished REtrived %d", objects.count);
            [[DataHolder DataHolderSharedInstance].MyObjectsTobeLiked removeAllObjects];
            [[DataHolder DataHolderSharedInstance].MyObjectsTobeLiked addObjectsFromArray:objects];
            [self DisplayUSerMedia];
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
            
        }
    }];
    act2.hidden=YES;
    reloadUserMedia.hidden=NO;
}



-(void)DisplayUSerMedia{

    NSLog(@"My count:%d",[DataHolder DataHolderSharedInstance].MediaObjectsArray.count);
    [self SegmentChanged:nil];
    self.view.userInteractionEnabled=YES;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 1;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.backgroundColor=[UIColor clearColor];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
     
        return 120;
    }
    return 79;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    int count=DataArray.count/4;
    if (DataArray.count%4!=0) {
        count++;
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"MyImageCell";
    CustomCell *cell = (CustomCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CustomCell" owner:self options:nil];
        for (id currentObject in topLevelObjects) {
            if ([currentObject isKindOfClass:[UITableViewCell class]]) {
                cell = (CustomCell *)currentObject;
                break;
            }
        }
    }
    cell.delegate=self;
    cell.callback=@selector(GoForLikes:);
    // Here we use the new provided setImageWithURL: method to load the web image
    if(4*indexPath.row+0<[DataArray count]){
        
        if (segment.selectedSegmentIndex==0) {
            MediaObject *Obj=[DataArray objectAtIndex:4*indexPath.row+0];
            cell.unv1.text=[NSString stringWithFormat:@"%@",Obj.likesCount];
            [cell.v1 setImageWithURL:[NSURL URLWithString:Obj.thumbnail]];
            cell.v1.clipsToBounds = YES;
            cell.b1.tag=4*indexPath.row+1;
        }
        else{
        
            PFObject *Obj=[DataArray objectAtIndex:4*indexPath.row+0];
            NSArray *arr=[Obj objectForKey:@"liked"];
            cell.unv1.text=[NSString stringWithFormat:@"%d/%@",arr.count,[Obj objectForKey:@"likesDue"]];
            [cell.v1 setImageWithURL:[NSURL URLWithString:[Obj objectForKey:@"url"]]];
            cell.v1.clipsToBounds = YES;
            cell.b1.hidden=YES;

        }
        
        
    }
    else{
        cell.v1.image=nil;
        cell.v1.hidden=YES;
        cell.b1.tag=0;
        cell.b1.hidden=YES;
        cell.unv1.hidden=YES;
    }
    if(4*indexPath.row+1<[DataArray count]){
        if (segment.selectedSegmentIndex==0) {
            MediaObject *Obj=[DataArray objectAtIndex:4*indexPath.row+1];
            cell.unv2.text=[NSString stringWithFormat:@"%@",Obj.likesCount];
            [cell.v2 setImageWithURL:[NSURL URLWithString:Obj.thumbnail]];
            cell.v2.clipsToBounds = YES;
            cell.b2.tag=4*indexPath.row+2;
        }
        else{
            
            PFObject *Obj=[DataArray objectAtIndex:4*indexPath.row+1];
            NSArray *arr=[Obj objectForKey:@"liked"];
            cell.unv2.text=[NSString stringWithFormat:@"%d/%@",arr.count,[Obj objectForKey:@"likesDue"]];
            [cell.v2 setImageWithURL:[NSURL URLWithString:[Obj objectForKey:@"url"]]];
            cell.v2.clipsToBounds = YES;
            cell.b2.hidden=YES;
            
        }
        
    }
    else{
        cell.v2.image=nil;
        cell.v2.hidden=YES;
        cell.b2.tag=0;
        cell.b2.hidden=YES;
        cell.unv2.hidden=YES;
    }
    if(4*indexPath.row+2<[DataArray count])
    {
        if (segment.selectedSegmentIndex==0) {
            MediaObject *Obj=[DataArray objectAtIndex:4*indexPath.row+2];
            cell.unv3.text=[NSString stringWithFormat:@"%@",Obj.likesCount];
            [cell.v3 setImageWithURL:[NSURL URLWithString:Obj.thumbnail]];
            cell.v3.clipsToBounds = YES;
            cell.b3.tag=4*indexPath.row+3;
        }
        else{
            
            PFObject *Obj=[DataArray objectAtIndex:4*indexPath.row+2];
            NSArray *arr=[Obj objectForKey:@"liked"];
            cell.unv3.text=[NSString stringWithFormat:@"%d/%@",arr.count,[Obj objectForKey:@"likesDue"]];
            [cell.v3 setImageWithURL:[NSURL URLWithString:[Obj objectForKey:@"url"]]];
            cell.v3.clipsToBounds = YES;
            cell.b3.hidden=YES;
            
        }
    }
    else{
        cell.v3.image=nil;
        cell.v3.hidden=YES;
        cell.b3.tag=0;
        cell.b3.hidden=YES;
        cell.unv3.hidden=YES;
    }
    if(4*indexPath.row+3<[DataArray count]){
        if (segment.selectedSegmentIndex==0) {
            MediaObject *Obj=[DataArray objectAtIndex:4*indexPath.row+3];
            cell.unv4.text=[NSString stringWithFormat:@"%@",Obj.likesCount];
            [cell.v4 setImageWithURL:[NSURL URLWithString:Obj.thumbnail]];
            cell.v4.clipsToBounds = YES;
            cell.b4.tag=4*indexPath.row+4;
        }
        else{
            
            PFObject *Obj=[DataArray objectAtIndex:4*indexPath.row+3];
            NSArray *arr=[Obj objectForKey:@"liked"];
            cell.unv4.text=[NSString stringWithFormat:@"%d/%@",arr.count,[Obj objectForKey:@"likesDue"]];
            [cell.v4 setImageWithURL:[NSURL URLWithString:[Obj objectForKey:@"url"]]];
            cell.v4.clipsToBounds = YES;
            cell.b4.hidden=YES;
            
        }
        
    }else{
        cell.v4.image=nil;
        cell.v4.hidden=YES;
        cell.b4.tag=0;
        cell.b4.hidden=YES;
        cell.unv4.hidden=YES;
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}
-(void)GoForLikes:(id)sender{

    NSLog(@"%d",[sender tag]);
    GetLikesViewController *Obj=[[GetLikesViewController alloc] initWithNibName:@"GetLikesViewController" bundle:nil];
    Obj.index=[sender tag]-1;
    [self presentViewController:Obj animated:YES completion:nil];
    //[self.navigationController pushViewController:Obj animated:YES];
}
-(IBAction)SegmentChanged:(id)sender{

    if (segment.selectedSegmentIndex==0) {
        DataArray=[DataHolder DataHolderSharedInstance].MediaObjectsArray;
        [table reloadData];
    }
    else{
    
        DataArray=[DataHolder DataHolderSharedInstance].MyObjectsTobeLiked;
        [table reloadData];
        
    }
    
}
-(IBAction)Logout:(id)sender{

    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Loagout" message:@"Are you sure want to logout?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [alert show];
    
    
}
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{

    if (buttonIndex==1) {
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        
        for(NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
            
            //if([[cookie domain] isEqualToString:@"https://instagram.com"]) {
            
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
            //}
        }
        [self dismissViewControllerAnimated:YES completion:nil];
        //[self.navigationController popToRootViewControllerAnimated:YES];
    }
}
-(IBAction)InApp:(id)sender{


    InAppPurchaseViewController *obj;
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        obj=[[InAppPurchaseViewController alloc] initWithNibName:@"InAppPurchaseViewController~iPad" bundle:nil];

    }else
    obj=[[InAppPurchaseViewController alloc] initWithNibName:@"InAppPurchaseViewController" bundle:nil];
   // [self.navigationController pushViewController:obj animated:YES];
    [self presentViewController:obj animated:NO completion:nil];
}

@end
