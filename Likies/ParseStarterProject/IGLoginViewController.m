//
//  IGLoginViewController.m
//  ParseStarterProject
//
//  Created by Kanwal on 4/21/14.
//
//

#import "IGLoginViewController.h"
#import "DataHolder.h"
#import "LikeViewController.h"

@interface IGLoginViewController ()

@end

@implementation IGLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //8ac69a0031f84f31b26b58b8b4dd7ce2
    NSString *fullURL = @"https://instagram.com/oauth/authorize/?client_id=8ac69a0031f84f31b26b58b8b4dd7ce2&redirect_uri=http://localhost:8888/MAMP/&response_type=token&scope=likes+comments+relationships";
    NSURL *url = [NSURL URLWithString:fullURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [mywebview loadRequest:requestObj];
    

    // Do any additional setup after loading the view from its nib.
}
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSString* urlString = [[request URL] absoluteString];
    //NSLog(@"Recieved String:::::::: %@", urlString);
    NSURL *Url = [request URL];
    NSArray *UrlParts = [Url pathComponents];
    // do any of the following here
    if ([[UrlParts objectAtIndex:(1)] isEqualToString:@"MAMP"]) {
        //if ([urlString hasPrefix: @"localhost"]) {
        NSRange tokenParam = [urlString rangeOfString: @"access_token="];
        if (tokenParam.location != NSNotFound) {
            NSString* token = [urlString substringFromIndex: NSMaxRange(tokenParam)];
            
            // If there are more args, don't include them in the token:
            NSRange endRange = [token rangeOfString: @"&"];
            if (endRange.location != NSNotFound)
                token = [token substringToIndex: endRange.location];
            
            NSLog(@"access token %@", token);
            if ([token length] > 0 ) {
                [DataHolder DataHolderSharedInstance].AccessToken=token;
                [DataHolder DataHolderSharedInstance].UserID=[[token componentsSeparatedByString:@"."] objectAtIndex:0];
                [self CehckRegistration];
            }
            // use delegate if you want
            //[self.delegate instagramLoginSucceededWithToken: token];
            
        }
        else {
            // Handle the access rejected case here.
            NSLog(@"rejected case, user denied request");
        }
        return NO;
    }
    return YES;
}
-(void)CehckRegistration{

    // Finds barbecue sauces that start with "Big Daddy's".
    PFQuery *query = [PFQuery queryWithClassName:@"user"];
    [query whereKey:@"userId" hasPrefix:[DataHolder DataHolderSharedInstance].UserID];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully retrieved %d scores.", objects.count);
            if(objects.count==0){
            
                [self RegisterUser];
                return;
            }
            // Do something with the found objects
            PFObject *object=[objects objectAtIndex:0];
            object[@"accessToken"]=[DataHolder DataHolderSharedInstance].AccessToken;
            [object saveInBackgroundWithTarget:self selector:@selector(GetListForLikes)];
            [DataHolder DataHolderSharedInstance].UserObjectID=object.objectId;
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
            //[self RegisterUser];
        }
    }];
    

}
-(void)RegisterUser{

    PFObject *user = [PFObject objectWithClassName:@"user"];
    user[@"userId"] = [DataHolder DataHolderSharedInstance].UserID;
    user[@"accessToken"] = [DataHolder DataHolderSharedInstance].AccessToken;
    user[@"coins"] = [NSNumber numberWithInt:0];
    
    PFACL *postACL = [PFACL ACLWithUser:[PFUser currentUser]];
    [postACL setPublicReadAccess:YES];
    [postACL setPublicWriteAccess:YES];
    user.ACL = postACL;
    
    [user saveInBackgroundWithTarget:self selector:@selector(GetListForLikes)];
    
}
-(void)GoNext{

    [[AppManager AppManagerSharedInstance] Show_Alert_With_Title:@"Instagram Login" message:@"You are successfully logged in"];
    
    LikeViewController *Obj=[[LikeViewController alloc] initWithNibName:@"LikeViewController" bundle:nil];
   // [self.navigationController pushViewController:Obj animated:YES];
    [self presentViewController:Obj animated:YES completion:nil];
}
-(void)GetListForLikes{
    
    PFQuery *query1 = [PFQuery queryWithClassName:@"user"];
    [query1 whereKey:@"userId" hasPrefix:[DataHolder DataHolderSharedInstance].UserID];
    [query1 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully retrieved %d scores.", objects.count);
            if(objects.count==0){
                
                [self RegisterUser];
                return;
            }
            // Do something with the found objects
            PFObject *object=[objects objectAtIndex:0];
            [DataHolder DataHolderSharedInstance].UserObjectID=object.objectId;
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
            //[self RegisterUser];
        }
    }];

    

    PFQuery *query = [PFQuery queryWithClassName:@"MediaLikes"];
    [query whereKey:@"liked" notEqualTo:[DataHolder DataHolderSharedInstance].UserID];
    [query whereKey:@"skip" notEqualTo:[DataHolder DataHolderSharedInstance].UserID];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully retrieved %d scores.", objects.count);
            [[DataHolder DataHolderSharedInstance].OBjectsTobeLiked removeAllObjects];
            [[DataHolder DataHolderSharedInstance].OBjectsTobeLiked addObjectsFromArray:objects];
            [self GoNext];
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
            //[self RegisterUser];
        }
    }];

}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
  [[AppManager AppManagerSharedInstance] Hide_Waiting_Alert];
    LoadingLabel.hidden=YES;
}
-(void)webViewDidStartLoad:(UIWebView *)webView{
    
    [[AppManager AppManagerSharedInstance] Show_Waiting_Alert];
    LoadingLabel.hidden=NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)Back:(id)sender{

    
}

@end
