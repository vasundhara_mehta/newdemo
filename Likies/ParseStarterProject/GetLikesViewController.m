//
//  GetLikesViewController.m
//  ParseStarterProject
//
//  Created by Kanwal on 4/22/14.
//
//

#import "GetLikesViewController.h"
#import "InAppPurchaseViewController.h"

@interface GetLikesViewController ()

@end

@implementation GetLikesViewController
@synthesize index;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        btn1.titleLabel.font  = [UIFont fontWithName:@"Dosis-Light" size:30];
        btn2.titleLabel.font  = [UIFont fontWithName:@"Dosis-Light" size:30];
        btn3.titleLabel.font  = [UIFont fontWithName:@"Dosis-Light" size:30];
        btn4.titleLabel.font  = [UIFont fontWithName:@"Dosis-Light" size:30];
        btn5.titleLabel.font  = [UIFont fontWithName:@"Dosis-Light" size:30];
        btn6.titleLabel.font  = [UIFont fontWithName:@"Dosis-Light" size:30];

    }else{
    btn1.titleLabel.font  = [UIFont fontWithName:@"Dosis-Light" size:13];
    btn2.titleLabel.font  = [UIFont fontWithName:@"Dosis-Light" size:13];
    btn3.titleLabel.font  = [UIFont fontWithName:@"Dosis-Light" size:13];
    btn4.titleLabel.font  = [UIFont fontWithName:@"Dosis-Light" size:13];
    btn5.titleLabel.font  = [UIFont fontWithName:@"Dosis-Light" size:13];
    btn6.titleLabel.font  = [UIFont fontWithName:@"Dosis-Light" size:13];
    }
    coinsLabel.layer.cornerRadius = 10;
    MediaObj=[[DataHolder DataHolderSharedInstance].MediaObjectsArray objectAtIndexedSubscript:index];
    [imageView setImageWithURL:[NSURL URLWithString:MediaObj.low_resolution]];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    [self UpdateCoinsLabel];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)GetLikes:(id)sender{

    act.hidden=NO;
    self.view.userInteractionEnabled=NO;
    if ([sender tag]==0) {
        [self GetLikedWithcoin:16 andLikes:10];
    }
    else if ([sender tag]==1) {
        [self GetLikedWithcoin:40 andLikes:25];
    }
    else if ([sender tag]==2) {
        [self GetLikedWithcoin:75 andLikes:50];
    }
    else if ([sender tag]==3) {
        [self GetLikedWithcoin:300 andLikes:200];
    }
    else if ([sender tag]==4) {
        [self GetLikedWithcoin:560 andLikes:400];
    }
    else if ([sender tag]==5) {
        [self GetLikedWithcoin:2800 andLikes:2000];
    }
}
-(void)GetLikedWithcoin:(int)coins andLikes:(int)likes{

    
    PFQuery *query = [PFQuery queryWithClassName:@"user"];
    PFObject *user= [query getObjectWithId:[DataHolder DataHolderSharedInstance].UserObjectID];
        int currentCoins=[[user objectForKey:@"coins"] integerValue]-CoinsUsed;
    if (currentCoins>coins) {
        CoinsUsed=coins;
        PFObject *gameScore = [PFObject objectWithClassName:@"MediaLikes"];
        gameScore[@"mediaId"] = MediaObj.Id;
        gameScore[@"likesDue"] = [NSNumber numberWithInt:likes];
        gameScore[@"userId"] = [DataHolder DataHolderSharedInstance].UserID;
        gameScore[@"url"] = MediaObj.low_resolution;
        
        PFACL *postACL = [PFACL ACLWithUser:[PFUser currentUser]];
        [postACL setPublicReadAccess:YES];
        [postACL setPublicWriteAccess:YES];
        gameScore.ACL = postACL;
        
        [gameScore saveInBackgroundWithTarget:self selector:@selector(useCoins)];
        [[AppManager AppManagerSharedInstance] Show_Alert_With_Title:@"Congratulations!" message:@"Your media will be start liking soon"];
        //[self.navigationController popViewControllerAnimated:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else{
    
        [[AppManager AppManagerSharedInstance] Show_Alert_With_Title:@"Sorry!" message:@"You have low coins"];

        [self InApp:nil];
        
    }
    act.hidden=YES;
    self.view.userInteractionEnabled=YES;
    
}
-(IBAction)InApp:(id)sender{

    InAppPurchaseViewController *obj=[[InAppPurchaseViewController alloc] initWithNibName:@"InAppPurchaseViewController" bundle:nil];
   // [self.navigationController pushViewController:obj animated:YES];
    [self presentViewController:obj animated:YES completion:nil];
}
-(void)useCoins{

    PFQuery *query = [PFQuery queryWithClassName:@"user"];
    [query getObjectInBackgroundWithId:[DataHolder DataHolderSharedInstance].UserObjectID block:^(PFObject *user, NSError *error) {
        // Do something with the returned PFObject in the gameScore variable.
        int coins=[[user objectForKey:@"coins"] integerValue]-CoinsUsed;
        user[@"coins"]=[NSNumber numberWithInt:coins];
        [user save];
    }];

}
-(IBAction)Back:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
    //[self.navigationController popViewControllerAnimated:YES];
}
-(void)UpdateCoinsLabel{
    
    PFQuery *query = [PFQuery queryWithClassName:@"user"];
    [query getObjectInBackgroundWithId:[DataHolder DataHolderSharedInstance].UserObjectID block:^(PFObject *user, NSError *error) {
        // Do something with the returned PFObject in the gameScore variable.
        int coins=[[user objectForKey:@"coins"] integerValue];
        coinsLabel.text=[NSString stringWithFormat:@" %d",coins];
    }];
    
}
@end
